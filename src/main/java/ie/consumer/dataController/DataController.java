package ie.consumer.dataController;


import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import ie.consumer.services.DataService;

@Controller
public class DataController {
	
	@Autowired
	DataService dataService;
	
	public QueryResult getData() {
		
		QueryResult queryResults = dataService.getData();
		
		return queryResults;
	}
	
	public QueryResult getAllData() {
		
		QueryResult queryResults = dataService.getAllData();
		
		return queryResults;
	}
}
