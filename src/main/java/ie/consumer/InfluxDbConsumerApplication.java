package ie.consumer;

import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ie.consumer.dataController.DataController;
import ie.consumer.services.KafkaProducerService;
import ie.consumer.services.MultiProducerService;
import ie.consumer.services.MultiProducerServiceImpl;

@SpringBootApplication
public class InfluxDbConsumerApplication implements CommandLineRunner {
	private static Logger logger = LoggerFactory.getLogger(InfluxDbConsumerApplication.class);

	@Autowired
	private DataController controller;

	@Autowired
	KafkaProducerService kafkaProducerService;
	
	@Autowired
	MultiProducerService multiProducerService;

	public static void main(String[] args) {
		SpringApplication.run(InfluxDbConsumerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		while (true) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//this method queries one sensor
			//QueryResult queryResults = controller.getData();

			//this method queries all sensors
			QueryResult queryResults = controller.getAllData();
			
			
			int counter = 1;
			for (QueryResult.Result result : queryResults.getResults()) {
				logger.info("counter: " + counter);
				
				int seriesSize = result.getSeries().size();
				logger.info("seriesSize: " + seriesSize);
				logger.info("queryResults: " + queryResults.getResults());
			}
			
			multiProducerService.produceTopics(queryResults);
			//kafkaProducerService.produce(queryResults);
		}
	}

}
