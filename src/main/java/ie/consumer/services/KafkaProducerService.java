package ie.consumer.services;

import org.influxdb.dto.QueryResult;

public interface KafkaProducerService {

	void produce(QueryResult queryResults);
}
