package ie.consumer.services;

import org.influxdb.dto.QueryResult;

public interface MultiProducerService {
	
	void produceTopics(QueryResult queryResults);
}
