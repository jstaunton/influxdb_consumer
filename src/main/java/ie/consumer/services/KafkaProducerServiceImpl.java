package ie.consumer.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.consumer.domain.Topic;
import ie.consumer.kafka.IKafkaConstants;
import ie.consumer.kafka.KafkaProducerCreator;
import kafka.utils.json.JsonObject;
import java.time.LocalDateTime;

@Service
public class KafkaProducerServiceImpl implements KafkaProducerService{
	private static Logger logger = LoggerFactory.getLogger(KafkaProducerServiceImpl.class);
	
	@Autowired
	Topic topic;
	String message = "";
	//JsonObject data;
	List<List<Object>> valuesList;
	
	@Override
	public void produce(QueryResult queryResults) {
		Producer<Long, String> producer = KafkaProducerCreator.createProducer();
		int counter = 1;
		for (QueryResult.Result result : queryResults.getResults()) {
			logger.info("counter: " + counter);
			
			int seriesSize = result.getSeries().size();
			logger.info("seriesSize: " + seriesSize);
			
			for(int i = 0; i < seriesSize; i++) {
				int seriesCounter = 1;
				logger.info("seriesCounter: " + seriesCounter);
				String seriesName = result.getSeries().get(i).getName();
				topic.setName(seriesName);
				logger.info("seriesName: " + seriesName);
	    		Map<String, String> tags = result.getSeries().get(i).getTags();
	    		if(tags != null) {
	        		for (Map.Entry<String, String> entry : tags.entrySet()) {
	        			logger.info(entry.getKey() + " = " + entry.getValue());
	        		}
	    		}
	    		
	    		List<String> columns = result.getSeries().get(i).getColumns();
	    		logger.info("****************************"+columns.get(0));
//	    		for(String column: columns) {
//	    			logger.info(column);
//	    		}
	    		
	    		valuesList = result.getSeries().get(i).getValues();
	    		
	    		for (int index = 0; index < IKafkaConstants.MESSAGE_COUNT; index++) {
		    		for(List<Object> values: valuesList) {
		    			for (int x=0; x < values.size();x +=columns.size()) {
		    				String message = "";
		    				for(int j = 0; j < columns.size(); j++) {
		    					//set time to now since time is set to 1970
		    					if (x+j == 0) {
		    						message += columns.get(j) + " " + LocalDateTime.now()  + " ";
		    					}
		    					else {
		    						message += columns.get(j) + " " +  values.get(x+j) + " ";	    						
		    					}
		    				}
		    				logger.info(message);
		    				ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(seriesName, message);
		    				try {
		  			          RecordMetadata metadata = producer.send(record).get();
		  			                      System.out.println("Record sent with key " + index + " to partition " + metadata.partition()
		  			                      + " with offset " + metadata.offset() + "to topic " + metadata.topic());
		  			               } 
		  			          catch (ExecutionException e) {
		  			                   System.out.println("Error in sending record");
		  			                   System.out.println(e);
		  			                } 
		  			          catch (InterruptedException e) {
		  		                    System.out.println("Error in sending record");
		  		                    System.out.println(e);
		  		                	}
		    				}
		    			}	    			
	    		}
//	    		for(List<Object> values: valuesList) {
//	    			int valuesListCounter = 1;
//					logger.info("valuesListCounter: " + valuesListCounter);
//	    			for(Object value: values) {
//	        			logger.info(value.toString());
//	        		}
//	    			valuesListCounter++;
//	    		}
	    		seriesCounter++;
//	            String timeString = (String) result.getSeries().get(i).getValues().get(0).get(0);
//	            logger.info(timeString);
			}
			counter++;
	    }
		
//		Producer<Long, String> producer = KafkaProducerCreator.createProducer();
//		  for (int index = 0; index < IKafkaConstants.MESSAGE_COUNT; index++) {
//			  for(List<Object> values: valuesList) {					  
//	    		for(Object value: values) {
//	    			ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(IKafkaConstants.TOPIC_NAME, value.toString());
//		        	//logger.info(value.toString());
//				  
////		              ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(IKafkaConstants.TOPIC_NAME, "This is another one of the records for influxDb " + index);
//			          try {
//			          RecordMetadata metadata = producer.send(record).get();
//			                      System.out.println("Record sent with key " + index + " to partition " + metadata.partition()
//			                      + " with offset " + metadata.offset());
//			               } 
//			          catch (ExecutionException e) {
//			                   System.out.println("Error in sending record");
//			                   System.out.println(e);
//			                } 
//			          catch (InterruptedException e) {
//		                    System.out.println("Error in sending record");
//		                    System.out.println(e);
//		                	}
//	    		}
//			  }
//	       }
	}

}
