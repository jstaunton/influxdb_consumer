package ie.consumer.services;

import java.nio.charset.StandardCharsets;
import org.apache.tomcat.util.codec.binary.Base64;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class DataServiceImpl implements DataService {
	private static Logger logger = LoggerFactory.getLogger(DataServiceImpl.class);

	@Override
	public QueryResult getData() {

		QueryResult queryResults = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8086/query?pretty=true&db=test1&q=SELECT LAST(*) FROM sensor1";
			// String URL = "http://localhost:8086/query?pretty=true&db=test7&q=SELECT *
			// FROM sensor1 WHERE humidity = 84";
			// String URL = "http://localhost:8087/api/sensorData";
			String userName = "${user}";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<QueryResult> myQuery = new ParameterizedTypeReference<QueryResult>() {
			};
			ResponseEntity<QueryResult> responseEntity = restTemplate.exchange(URL, HttpMethod.GET,
					new HttpEntity<>(headers), myQuery);
			queryResults = responseEntity.getBody();

			logger.info("queryResults: " + queryResults.getResults());

		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("error!!!: " + ex.getMessage());
		}

		return queryResults;

	}

	@Override
	public QueryResult getAllData() {

		QueryResult queryResults = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8086/query?pretty=true&db=test1&q=SELECT LAST(*) FROM /sensor/";
			String userName = "${user}";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<QueryResult> myQuery = new ParameterizedTypeReference<QueryResult>() {
			};
			ResponseEntity<QueryResult> responseEntity = restTemplate.exchange(URL, HttpMethod.GET,
					new HttpEntity<>(headers), myQuery);
			queryResults = responseEntity.getBody();

			//logger.info("queryResults: " + queryResults.getResults());

		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("error!!!: " + ex.getMessage());
		}

		return queryResults;

	}
	private HttpHeaders createHeaders(String userName, String password) {

		return new HttpHeaders() {
			{
				String auth = userName + ":" + password;
				byte[] encodeStringIntoBytes = auth.getBytes(StandardCharsets.UTF_8);
				byte[] encodedAuth = Base64.encodeBase64(encodeStringIntoBytes);
				String authHeader = "Basic " + new String(encodedAuth);
				logger.info("INFO...{}", authHeader);
				set(HttpHeaders.AUTHORIZATION, authHeader);
			}
		};
	}

}
