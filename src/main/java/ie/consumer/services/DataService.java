package ie.consumer.services;

import org.influxdb.dto.QueryResult;

public interface DataService {

	QueryResult getData();
	QueryResult getAllData();
}
