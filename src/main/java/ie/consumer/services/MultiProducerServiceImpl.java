package ie.consumer.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.influxdb.dto.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.consumer.domain.Topic;
import ie.consumer.kafka.IKafkaConstants;
import ie.consumer.kafka.KafkaProducerCreator;

@Service
public class MultiProducerServiceImpl implements MultiProducerService{
	private static Logger logger = LoggerFactory.getLogger(KafkaProducerServiceImpl.class);
	
	@Autowired
	Topic topic;
	List<List<Object>> valuesList;
	
	@Override
	public void produceTopics(QueryResult queryResults) {
		Producer<Long, String> producer = KafkaProducerCreator.createProducer();
		
		for (QueryResult.Result result : queryResults.getResults()) {
			int seriesSize = result.getSeries().size();
			
			for(int i = 0; i < seriesSize; i++) {
				
				String seriesName = result.getSeries().get(i).getName();
				topic.setName(seriesName);//not needed???
				logger.info("seriesName: " + seriesName);
	    		Map<String, String> tags = result.getSeries().get(i).getTags();
	    		if(tags != null) {
	        		for (Map.Entry<String, String> entry : tags.entrySet()) {
	        			logger.info(entry.getKey() + " = " + entry.getValue());
	        		}
	    		}
	    		
	    		List<String> columns = result.getSeries().get(i).getColumns();
	    		
	    		valuesList = result.getSeries().get(i).getValues();
	    		
	    		for (int index = 0; index < IKafkaConstants.MESSAGE_COUNT; index++) {//unnecessary loop, i've set count = 1 ???
	    			for(List<Object> values: valuesList) {
	    				for (int x=0; x < values.size();x +=columns.size()) {
		    				String message = "";
		    				for(int j = 0; j < columns.size(); j++) {
		    					//set time to now since time is set to 1970
		    					if (x+j == 0) {
		    						message += columns.get(j) + " " + LocalDateTime.now()  + " ";
		    					}
		    					else {
		    						message += columns.get(j) + " " +  values.get(x+j) + " ";	    						
		    					}	    							
		    				}
		    				logger.info("MultiProducerServiceImpl: values parsed from influx query: " + message);
		    				ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(seriesName, message);
		    				try {
		  			          RecordMetadata metadata = producer.send(record).get();
		  			                      System.out.println("Record sent with key " + index + " to partition " + metadata.partition()
		  			                      + " with offset " + metadata.offset() + "to topic " + metadata.topic());
		  			               } 
		  			          catch (ExecutionException e) {
		  			                   System.out.println("Error in sending record");
		  			                   System.out.println(e);
		  			                } 
		  			          catch (InterruptedException e) {
		  		                    System.out.println("Error in sending record");
		  		                    System.out.println(e);
		  		                	}
		    				}
	    			}
	    		}
			}
		}
	}

}
